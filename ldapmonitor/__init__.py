import os
from threading import Thread
import time
import logging
import configparser
import importlib
import pkgutil

class ConfigException(Exception):
    pass

class DSMonitorConfigParser(configparser.ConfigParser):

    def __init__(self, config_file):
        super().__init__()

        self.logger = logging.getLogger()

        if os.path.isabs(config_file):
            self.logger.info(f"config files: {config_file}")
            self.read(config_file)
        else:
            configpath = []
            if os.environ.get('APPDATA'):
                configpath.append(os.path.join(os.environ.get('APPDATA'),config_file))
            if os.environ.get('XDG_CONFIG_HOME'):
                configpath.append(os.path.join(os.environ.get('XDG_CONFIG_HOME'),config_file))
            if os.environ.get('HOME'):
                configpath.append(os.path.join(os.environ.get('HOME'),f".{config_file}"))
            configpath.append(os.path.abspath(config_file))

            found = self.read(configpath)
            self.logger.info(f"config files: {found}")
        self.validate_config()
    
    def validate_config(self):
        required_values = {
            'main' : {
                'net_timeout': None
            }
        }
        required_values_389ds = {
            'bind_uri': None,
            'bind_dn': None,
            'bind_pw': None,
            'basedn': None
        }

        # Allgemeiner Check auf die "festen" Sektionen
        for section, keys in required_values.items():
            if section not in self:
                raise ConfigException(
                    f"Missing section {section} in the config file")

            for key, values in keys.items():
                if key not in self[section] or self[section][key] == '':
                    raise ConfigException(
                        f'Missing value for {key} under section {section} in the config file')

                if values != None:
                    if self[section][key] not in values:
                        raise ConfigException(
                            f'Invalid value for {key} under section {section} in the config file')

        # Check auf die dynamischen Sektionen fuer die Server-Verbindungen
        for section in self.sections():
            if section[0:len('389-ds:')] == '389-ds:':
                for key, values in required_values_389ds:
                    if key not in self.sections[section] or self.sections[section][key] == '':
                        raise ConfigException(
                            f"Missing value for {key} under section {section} in the config file"
                        )
                    if values != None:
                        if self[section][key] not in values:
                            raise ConfigException(
                                f"Invalid value for {key} unter section {section} in the config file"
                            )

def clear():
    if os.name == 'nt':
        _ = os.system("cls")
    else:
        _ = os.system("clear")


