
import logging
import threading
import ldap
from ldap.dn import escape_dn_chars
import time
import ssl
import json

from ldapmonitor.data import DSMonitorData
import ldapmonitor


def createProbeWorker(*args, **kwargs):
    return ProbeWorker(*args, **kwargs)

class ProbeWorker(threading.Thread):

    """Datenspeicher Objekt"""
    data = None

    def __init__(self, group=None, target=None, name=None,
        args=(), kwargs=None, verbose=None, daemon=False):
        super().__init__(group=group, target=target,
            name=name, daemon=daemon)
        self.args = args
        self.kwargs = kwargs

        self.data = DSMonitorData()
        self.repl_agmt = dict()

        self._kill = threading.Event()
        self._interval = 1


    def kill(self):
        self._kill.set()

    def run(self):
        while True:
            self.singleCheck()
            if self._kill.wait(self._interval):
                break

    def singleCheck(self):
        conn = ldap.initialize(self.kwargs['bind_uri'])
        try:
            conn.protocol_version = ldap.VERSION3
            cafile = ssl.get_default_verify_paths().cafile
            conn.set_option(ldap.OPT_X_TLS_CACERTFILE,cafile)
            conn.set_option(ldap.OPT_X_TLS_NEWCTX,0)
            conn.set_option(ldap.OPT_NETWORK_TIMEOUT,int(self.kwargs['net_timeout']))
            #conn.start_tls_s()
            conn.simple_bind_s(self.kwargs['bind_dn'],self.kwargs['bind_pw'])
            self.data.set('status','U')

            # Abfrage des Benutzernames
            whoami = conn.whoami_s()
            self.data.set('whoami',whoami)
            
            
            # Abrage des cn=config Daten
            config_attr_mapping = {
                'nsslapd-localhost': 'hostname',
                'nsslapd-versionstring': 'version',
                'nsslapd-defaultnamingcontext': 'defaultcontext'
            }
            r = conn.search_s(
                'cn=config',
                ldap.SCOPE_BASE,
                attrlist=config_attr_mapping.keys()
                )
            for dn, robj in r:
                for key, name in config_attr_mapping.items():
                    if key in robj:
                        self.data.set(name,robj[key][0].decode())
                    else:
                        logging.error(f"No Value for {key} found in cn=config")

            # Abrage der BaseDN Daten
            basedn_attr_mapping = {
                'entrydn': 'basedn',
                'nsuniqueid': 'root_dse_id',
                'numsubordinates': 'root_dse_numchilds'
            }
            r = conn.search_s(
                self.kwargs['basedn'],
                ldap.SCOPE_BASE,
                attrlist=basedn_attr_mapping.keys()
                )
            for dn, robj in r:
                for key, name in basedn_attr_mapping.items():
                    if key in robj:
                        self.data.set(name,robj[key][0].decode())
                    else:
                        logging.error(f"No Value for {key} found in cn=config")

            # Abrage desMonitorings
            monitor_attr_mapping = {
                'currenttime': 'monitor_currenttime'
            }
            r = conn.search_s(
                "cn=monitor",
                ldap.SCOPE_BASE,
                attrlist=monitor_attr_mapping.keys()
                )
            for dn, robj in r:
                for key, name in monitor_attr_mapping.items():
                    if key in robj:
                        self.data.set(name,robj[key][0].decode())
                    else:
                        logging.error(f"No Value for {key} found in cn=config")

            # Abfrage der Replikations-Daten
            repl_base_attr_mapping = {
                'nsds5replicaid': 'repl_id',
                'nsds5replicabinddn': 'repl_bind_dn'
            }
            repl_base_obj_dn = f"cn=replica,cn={ escape_dn_chars(self.kwargs['basedn']) },cn=mapping tree,cn=config"

            r = conn.search_s(
                repl_base_obj_dn,
                ldap.SCOPE_BASE,
                attrlist=repl_base_attr_mapping.keys()
                )
            for dn, robj in r:
                for key, name in repl_base_attr_mapping.items():
                    if key in robj:
                        self.data.set(name,robj[key][0].decode())
                    else:
                        logging.error(f"No Value for {key} found in {repl_base_obj_dn}")
            
            # Abfragen der einzelnen Repl-Agmt-Daten
            repl_agmt_attr_mapping = {
                'cn': 'repl_agmt_name',
                'nsds5replicalastupdateend': 'repl_agmt_last_update',
                'nsds5replicalastupdatestatusjson': 'repl_agmt_status_json'
            }
            r = conn.search_s(
                repl_base_obj_dn,
                ldap.SCOPE_ONELEVEL,
                attrlist=repl_agmt_attr_mapping.keys()
                )
            found_repl_agmt = []
            for dn, robj in r:
                found_repl_agmt.append(dn)
                if dn not in self.repl_agmt:
                    self.repl_agmt[dn] = DSMonitorData()
                for key, name in repl_agmt_attr_mapping.items():
                    if key in robj:
                        self.repl_agmt[dn].set(name,robj[key][0].decode())
                    else:
                        logging.error(f"No Value for {key} found in {dn}")
            # Cleanup old Repl-Agmt status
            for dn in self.repl_agmt.keys():
                if dn not in found_repl_agmt:
                    self.repl_agmt[dn].cleanup()
        except ldap.SERVER_DOWN as e:
            self.data.cleanup()
            for dn in self.repl_agmt.keys():
                self.repl_agmt[dn].cleanup()
            self.data.set('status','D')
        except ldap.INVALID_CREDENTIALS as e:
            logging.error(e)
        except ldap.NO_SUCH_OBJECT as e:
            logging.error(e)
        except ldap.LDAPError as e:
            logging.error(e)
        finally:
            conn.unbind()

    def display(self):
        hostname = str(self.data.get('hostname'))
        version = str(self.data.get('version'))
        basedn = str(self.data.get('basedn'))
        root_dse_id = str(self.data.get('root_dse_id'))
        root_dse_numchilds = f"Childs: {str(self.data.get('root_dse_numchilds')):>5}"
        status = "N"
        if self.data.get('status') != None:
            status = str(self.data.get('status'))

        repl_id = str(self.data.get('repl_id'))
        repl_bind_dn = str(self.data.get('repl_bind_dn'))

        monitor_currenttime = str(self.data.get("monitor_currenttime"))

        print("-" * 72)
        print(f"{hostname:<40} {status} {version:>29}")
        print(f"  {basedn:<50}{monitor_currenttime:>20}")
        print(f"  {root_dse_id:<40}{root_dse_numchilds:>30}")
        if repl_id != None:
            print(f"  Repl-ID: {repl_id:<10}{repl_bind_dn:>51}")
            for repl_agmt_dn in self.repl_agmt.keys():
                data = self.repl_agmt[repl_agmt_dn]
                repl_agmt_name = str(data.get('repl_agmt_name'))
                repl_agmt_last_update = str(data.get('repl_agmt_last_update'))
                j_state = ""
                try:
                    j_obj = data.get('repl_agmt_status_json')
                    if j_obj != None:
                        j = json.loads( str(j_obj))
                        j_state = j['state']
                except SyntaxError as e:
                    logging.error(e, exc_info=True)
                print(f"    Repl-ID: {repl_agmt_name:<30}{repl_agmt_last_update:<20}{j_state:>9}")
