
import time
import logging

class DSMonitorData():

    """Daten aus diesem Thread"""
    _data = None

    """Zeitpunkte der letzen Datenupdates"""
    _lastupdate = None

    def __init__(self):
        self._data = dict()
        self._lastupdate = dict()

    def cleanup(self):
        self._data = dict()
        self._lastupdate = dict()

    def set(self,key,value):
        self._data[key] = value
        self._lastupdate[key] = time.time()
        logging.debug(f"setting {key} to {value}")

    def get(self,key):
        _ret = None
        if key in self._data:
            _ret = self._data[key]
        return _ret

