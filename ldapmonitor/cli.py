#!/usr/bin/python3

import pkg_resources
import os
import logging
import configparser
import argparse
import sys
import time

import ldapmonitor


def validate_config(config_filename = "app.ini"):
    config = False
    try:
        config = ldapmonitor.DSMonitorConfigParser(config_filename)
    except ldapmonitor.ConfigException as e:
        print(e)
        logging.getLogger().error(e)
        return False
    return config

def getMyOutputers():
    # Find all my outputers
    eps = pkg_resources.iter_entry_points(group='ldapmonitor.probe_plugins')

    # Mapping for all my outputers
    outputers = {
        entrypoint.name: entrypoint
        for entrypoint in eps
    }
    return outputers

def main() -> int:
    logging.basicConfig(level=logging.INFO, 
        format='%(asctime)s - (%(threadName)-9s) - %(name)s - %(levelname)s - %(message)s',
        filename="app.log", filemode='w'
    )

    parser = argparse.ArgumentParser(
        description = "top like monitor for LDAP-Servers"
    )
    parser.add_argument("--config","-c", action="store", dest="config", help="configuration file", default=None)

    args = parser.parse_args()

    if args.config != None:
        cfg_file = os.path.abspath(args.config)
    else:
        cfg_file = "ldapmonitor.cfg"

    config = validate_config(cfg_file)
    if config == False:
        sys.exit(1)

    outputers = getMyOutputers()

    
    threads = []
    for section in config.sections():
        if section[0:4] == 'dss:':
            try:
                cfg = dict(config.items(section))
                cfg['net_timeout'] = config['main']['net_timeout']

                # get the one outputer specified by --outputer
                try:
                    outputer = outputers[ "dirsrv" ].load()
                except KeyError:
                    print(f'outputer dirsrv is not available', file=sys.stderr)
                    print(f'available outputers: {", ".join(sorted(outputers))}')
                    return 1

                # run the outputer plugin
                t = outputer(
                    daemon=True,
                    kwargs=cfg
                    )
                threads.append(t)
                t.start()
                print(t)
            except Exception as e:
                logging.error(e, exc_info=True)
    try:
        if len(threads) >= 0:
            ctr = 6
            while ctr > 0:
                ldapmonitor.clear()
                for t in threads:
                    t.display()
                    
                time.sleep(config.get('main','wait', fallback=1))
                if 'developermode' in config['main']:
                    ctr = ctr -1
    except KeyboardInterrupt:
        pass
    except Exception as e:
        logging.error(e, exc_info=True)
    return 0

if __name__ == '__main__':
    main()
