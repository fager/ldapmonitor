= LDAP Monitor

Live status monitoring for LDAP-Server

== Development on Fedora

[source,bash]
----
sudo dnf install openldap-devel python3-devel python3-ldap tox
----

Install in user-space with: ```pip install --user -e .```

