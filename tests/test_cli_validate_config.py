from ldapmonitor.cli import validate_config

def test_validate_config_ok():
    config = validate_config("tests/data/ok.ini")
    assert config != False

def test_validate_config_failed():
    config = validate_config("tests/data/fail.ini")
    assert config == False
